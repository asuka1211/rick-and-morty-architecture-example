package com.serma.rickandmorty.presentation.character

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import by.kirich1409.viewbindingdelegate.viewBinding
import com.bumptech.glide.Glide
import com.serma.rickandmorty.R
import com.serma.rickandmorty.databinding.FragmentCharacterBinding
import com.serma.rickandmorty.di.DiContainer
import com.serma.rickandmorty.domain.RandomCharacterUseCase
import com.serma.rickandmorty.presentation.character.mapper.toPersonUi
import com.serma.rickandmorty.presentation.character.model.CharacterStatusUi
import com.serma.rickandmorty.presentation.character.model.PersonUi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class CharacterFragment : Fragment(R.layout.fragment_character) {

    private val binding: FragmentCharacterBinding by viewBinding(FragmentCharacterBinding::bind)
    private val useCase: RandomCharacterUseCase = DiContainer.randomCharacterUseCase

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (savedInstanceState == null) {
            loadData(0)
        }
    }

    private fun loadData(page: Int) {
        lifecycleScope.launch {
            runCatching {
                useCase.invoke(page)
            }.fold(onSuccess = { render(it.toPersonUi()) }, onFailure = {
                showErrorDialog(it.message, page)
            })
        }
    }

    private fun render(data: PersonUi) {
        with(binding) {
            Glide.with(avatar).load(data.imageUrl).into(avatar)
            name.text = data.name
            status.setImageResource(
                when (data.status) {
                    is CharacterStatusUi.Alive -> R.drawable.alive_ic
                    is CharacterStatusUi.Dead -> R.drawable.dead_ic
                    is CharacterStatusUi.Unknown -> R.drawable.unknown_ic
                }
            )
            type.text = getString(R.string.type, getString(data.status.statusRes), data.species)
            firstSeenIn.text = getString(R.string.first_seen_in, data.firstSeenIn)
            lastSeenLocation.text = getString(R.string.last_seen_location, data.lastLocation)
            prevItem.isVisible = data.page != 0
            prevItem.setOnClickListener {
                loadData(data.page - 1)
            }
            nextItem.setOnClickListener {
                loadData(data.page + 1)
            }
        }
    }

    private fun showErrorDialog(errorMessage: String?, page: Int) {
        AlertDialog.Builder(requireContext())
            .setTitle(R.string.error_title)
            .setMessage(errorMessage)
            .setPositiveButton(getText(R.string.retry_button)) { dialog, which ->
                loadData(page)
            }
            .setCancelable(false)
            .show()
    }
}